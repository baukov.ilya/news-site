import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchStateAction} from "./actions/state";
import Navbar from "./components/nav-bar";
import States from "./components/states";
import Authors from "./components/authors";
import MainPage from "./components/main_page";
import Author from "./components/authorPage";
import Footer from "./components/footer";
import Search from "./components/searchPage";
import State from "./components/state";
import { BrowserRouter, Route} from "react-router-dom";
class App extends Component{

  render() {
    return(
        <BrowserRouter>
          <Navbar/>
          <Route  component={States} path='/states'/>
          <Route component={Authors} path='/authors'/>
          <Route exact component={MainPage} path='/'/>
          <Route component = {Author} path = '/author'/>
          <Route component = {Search} path = '/search'/>
          <Route component = {State} path = '/state'/>
          <Footer/>
        </BrowserRouter>
    )

  }
}

export default App