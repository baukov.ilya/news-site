
export function fetchStateAction(lastNumber, selection) {
    return async(dispatch, getState) => {

        dispatch({
            type:"FETCH_STATES"
        });


        const response = await fetch("http://dvv2423.fvds.ru/api/post/" + selection + "/1-" + lastNumber);
        const states = await response.json();
        console.log(states);

        dispatch({
            type: 'FETCH_STATES_SUCCESS',
            states
        });

        console.log(getState());
    }
}
