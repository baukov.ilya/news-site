const initialState = {
    states:[],
    isFetching: false
};

function stateReducer(state = initialState, action) {
    console.log(action);
    console.log(state);
    switch(action.type){
        case 'FETCH_STATES_SUCCESS':
            return {
                ...state,
                states: action.states,
                isFetching: false
            };
        case "FETCH_STATES":
            return{
                ...state,
                isFetching: true
            };
        default:
            return state;
    }
}
export default stateReducer;