import React, { Component } from 'react';
import AuthorState from "../authorPage/author";

class Search extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded1: false,
            items: [],
            state_count: 4,
            categories: [],
            id: 0
        };
    }

    componentDidMount() {
        const href = window.location.href.split("?=");
        const id = href[href.length - 1];
        this.setState({
            id: id
        })
            fetch("http://thevyshka.styleru.org/api/post/search/"+ id +"/1-4")
                .then((response) => {
                    return response.json();
                })
                .then((data) => {
                    this.setState({
                        isLoaded1: true,
                        items: data.posts
                    });
                });

    }


    render() {
        const { error, isLoaded1, items, categories} = this.state;
        const href = decodeURI(window.location.href).split("=");
        const id = href[href.length - 1];
        console.log(id)
        if(isLoaded1){
            return(
                <div className="author_page">
                    <div className="page_content">
                        <div className="states__header">Поиск :: {id}</div>
                        <div className="author__states">
                            {this.renderStates()}
                        </div>
                        <div className="button_container" onClick={() => this.UploadState()}>
                            <div className="button" >
                                <div className="button__content">Больше статей</div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        else{
            return (
                <div className="loading">Loading...</div>
            )
        }

    }

    renderStates(){
        const { error, isLoaded1, items} = this.state;
        console.log(items);
        if( isLoaded1) {
        return(
            items.map((item) =>{
                return(
                    <AuthorState data = {item}/>
                )
            }))
        }
        else{
            return (
                <div> states not founded </div>
            )
        }
    }

    UploadState(){
        const { state_count } = this.state;
        let cnt = state_count + 4;
        this.setState({
            state_count: cnt,
        });
        console.log(this.state.state_count);
        const href = window.location.href.split("?=");
        const id = href[href.length - 1];
        fetch("http://thevyshka.styleru.org/api/post/search/"+ id +"/1-" + cnt )
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded2: true,
                    items: data.posts,
                });
            });
    }


}

export default Search