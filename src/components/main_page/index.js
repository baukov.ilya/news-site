import React, { Component } from 'react';
import Casual_state from "./../states/Casual_state";
import Dark_casual_state from "./../states/Dark_casual_state";
import Big_state from "./../states/Big_state";
import News from "./../states/news";

class MainPage extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded1: false,
            items: [],
            state_count: 19
        };
    }

    componentDidMount() {
        fetch('http://thevyshka.styleru.org/api/post/published/1-9')
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded1: true,
                    items: data.posts
                });
            });
    }

    render() {
        const { error, isLoaded1, items} = this.state;
        if(isLoaded1) {
            return (
                <div className="states_page">
                    <div className={"states"}>
                        <News/>
                        {this.renderStates()}
                        <div className="button_container" onClick={() => this.UploadState()}>
                            <div className="button">
                                <div className="button__content">Больше статей</div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        else{
            return (
                <div>Loading....</div>
            )
        }

    }
    renderStates(){
        const { items, isLoaded1, state_count, allLoaded} = this.state;
        if( isLoaded1) {
                return (
                    items.map((state, cnt) => {
                        if (Math.floor((cnt + 1)/10) !== 0) {
                            if ((cnt + 1) % 10 == 0 || (cnt + 1) % 10 == 3 || (cnt + 1) % 10 == 6 || (cnt + 1) % 10 == 7 || cnt % 10 == 9)
                                return (
                                    <Casual_state state={items[cnt]}/>
                                )
                            else if ((cnt + 1) % 10 == 2 || (cnt + 1) % 10 == 4 || (cnt + 1) % 10 == 8 || (cnt + 1) % 10 == 9)
                                return (
                                    <Dark_casual_state state={items[cnt]}/>
                                )
                            else
                                return (
                                    <Big_state state={items[cnt]}/>
                                )
                        }
                        else
                            if(cnt % 10 == 1 || cnt % 10 == 3 ||  cnt % 10 == 5 || cnt % 10 == 6 )
                                return (
                                    <Casual_state state = {items[cnt]}/>
                                )
                            else if(cnt % 10 == 2 || cnt % 10 == 7 || cnt % 10 == 8)
                                return (
                                    <Dark_casual_state state = {items[cnt]}/>
                                )
                            else
                                return (
                                    <Big_state state = {items[cnt]}/>
                                )

                    })
                )
        }
        else{
            return (
                <div> states not founded </div>
            )
        }
    }
    UploadState(){
        const { state_count } = this.state;
        let cnt = state_count + 10;
        this.setState({
            state_count: cnt,
        });
        console.log(this.state.state_count);
        fetch('http://thevyshka.styleru.org/api/post/published/1-' + this.state.state_count )
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded2: true,
                    items: data.posts,
                });
            });
        console.log(this.state.items)
    }
}

export default MainPage