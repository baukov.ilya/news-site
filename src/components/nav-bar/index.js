import React from 'react';
import './style.css';
import logo from '../pics/TheVyshka logo 2.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from "@fortawesome/free-solid-svg-icons";

class Navbar extends React.Component{
    render() {
        return(
            <div className="nav">
                <a href="/"><img className="logo" src={logo} alt="logo"></img></a>
                <div className="nav__screen-shadow"></div>
                <div className="nav__desktop">
                    <div className="nav__search">
                        <input className={"nav__search-input"} type="text" placeholder={"Поиск"} onChange={this.Search}/>
                        <a className="nav__search-linkbtn" href="/search?=ВШЭ">
                            <button className={"nav__search-input-button"} >
                                <svg width="36" height="32" viewBox="0 0 36 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M24.5173 21.1027L34 30M28.1797 14.2818C28.1797 21.0649 22.3192 26.5637 15.0899 26.5637C7.86053 26.5637 2 21.0649 2 14.2818C2 7.49877 7.86053 2 15.0899 2C22.3192 2 28.1797 7.49877 28.1797 14.2818Z" stroke="white" stroke-width="3"/>
                                </svg>
                            </button>
                        </a>
                        <div className="nav__search-exit" onClick={HideSearch}>
                            <button>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M22.9079 23L1 1.80175M1.09207 22.1982L23 1" stroke="#909090" stroke-width="2"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div className="nav__list">
                        <div className="nav__list__item" onClick={ShowStates}>
                            <div className="nav__list__item-name" >Рубрики </div>
                            <div className="nav__list__item-button">
                                <svg width="8" height="5" viewBox="0 0 8 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M4 5L0.535898 0.5L7.4641 0.5L4 5Z" fill="white"/>
                                </svg>
                            </div>

                            <div className="nav__list__item-hidden__square"></div>
                            <div className="nav__list__item-hidden">
                                <a href="/states/news"><div className="nav__list__item-hidden__title">
                                    Новости
                                </div></a>
                                <a href="/states/opinions"><div className="nav__list__item-hidden__title">
                                    Мнения
                                </div></a>
                                <a href="/states/invisible"><div className="nav__list__item-hidden__title">
                                    Невидимая вышка
                                </div></a>
                                <a href="/states/places"><div className="nav__list__item-hidden__title">
                                    Места
                                </div></a>
                                <a href="/states/histories"><div className="nav__list__item-hidden__title">
                                    Истории
                                </div></a>

                            </div>
                        </div>
                        <div className="nav__list__item">
                            <a href="/states/special"><div className="nav__list__item-name">Спецпроекты </div></a>
                        </div>
                        <div className="nav__list__item">
                            <a href="/states/photo"><div className="nav__list__item-name">Фотопроекты </div></a>
                        </div>
                        <div className="nav__list__item">
                            <a href="/states/podcasts"><div className="nav__list__item-name">Подкасты</div></a>
                        </div>
                        <div className="nav__list__item" onClick={ShowStates2}>
                           <div className="nav__list__item-name">Редакция </div>
                            <div className="nav__list__item-button">
                                <svg width="8" height="5" viewBox="0 0 8 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4 5L0.535898 0.5L7.4641 0.5L4 5Z" fill="white"/>
                                </svg>
                            </div>
                            <div className="nav__list__item-hidden__square"></div>
                            <div className="nav__list__item-hidden">
                                <a href=""><div className="nav__list__item-hidden__title">
                                    О нас
                                </div></a>
                                <a href="/authors"><div className="nav__list__item-hidden__title">
                                    Авторы
                                </div></a>
                            </div>
                        </div>

                        <div className="nav__list__item-search__button" onClick={showSearch}>
                            <svg width="36" height="32" viewBox="0 0 36 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M24.5173 21.1027L34 30M28.1797 14.2818C28.1797 21.0649 22.3192 26.5637 15.0899 26.5637C7.86053 26.5637 2 21.0649 2 14.2818C2 7.49877 7.86053 2 15.0899 2C22.3192 2 28.1797 7.49877 28.1797 14.2818Z" stroke="white" stroke-width="3"/>
                            </svg>

                        </div>
                    </div>
                </div>
                <div className="nav__mobile">
                    <div className="nav__mobile-button" onClick={ShowNav}>
                        <FontAwesomeIcon icon={faBars} />
                    </div>
                    <div className="nav__mobile-button-exit" onClick={HideNav}>
                        <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M24.8996 25L1 1.87464M1.10044 24.1254L25 1" stroke="white" stroke-width="2"/>
                        </svg>
                    </div>
                    <div className="nav__mobile-items">
                        <div className="nav__mobile-items__search">
                            <input type="text" placeholder={"Поиск"}/>
                        </div>
                        <div className="nav__mobile-item" >
                            <div className="nav__mobile-item-title" onClick={ShowState3}>Рyрики
                                <div className="nav__mobile-item-more">
                                    <svg width="8" height="5" viewBox="0 0 8 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M4 5L0.535898 0.5L7.4641 0.5L4 5Z" fill="#7D2995"/>
                                    </svg>
                                </div>
                            </div>
                            <div className="nav__mobile-item-subtitles">
                                <a href="/states/news"><div className="nav__mobile-item-subtitle">Новости</div></a>
                                <a href="/states/opinions"><div className="nav__mobile-item-subtitle">Мнения</div></a>
                                <a href="/states/invisible"><div className="nav__mobile-item-subtitle">Невидимая Вышка</div></a>
                                <a href="/states/places"><div className="nav__mobile-item-subtitle">Места</div></a>
                                <a href="/states/histories"><div className="nav__mobile-item-subtitle">Истории</div></a>
                            </div>
                        </div>
                        <div className="nav__mobile-item">
                            <a href="/states/special"><div className="nav__mobile-item-title">Спецпроекты</div></a>
                            <a href="/states/photo"><div className="nav__mobile-item-title">Фотопроекты</div></a>
                            <a href="/states/podcasts"><div className="nav__mobile-item-title">Подкасты</div></a>
                        </div>
                        <div className="nav__mobile-item" >
                            <div className="nav__mobile-item-title" onClick={ShowState4}>Редакция
                                <div className="nav__mobile-item-more">
                                    <svg width="8" height="5" viewBox="0 0 8 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M4 5L0.535898 0.5L7.4641 0.5L4 5Z" fill="#7D2995"/>
                                    </svg>
                                </div>
                            </div>
                            <div className="nav__mobile-item-subtitles">
                                <a href="/authors"><div className="nav__mobile-item-subtitle">Авторы</div></a>
                                <div className="nav__mobile-item-subtitle">О нас</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    Search(){
     const input = document.getElementsByClassName("nav__search-input")[0].value;
     const link = document.getElementsByClassName("nav__search-linkbtn")[0];
     link.href = "/search?=" + input;
    }
}

function showSearch() {
    let logo = document.getElementsByClassName("logo")[0];
    let list = document.getElementsByClassName("nav__list")[0];
    let SearchWindow = document.getElementsByClassName("nav__search")[0];
    logo.style.display = "none";
    list.style.display = "none";
    SearchWindow.style.display = "inline-flex";
    let shadow = document.getElementsByClassName("nav__screen-shadow")[0];
    shadow.style.height = document.documentElement.scrollHeight - 100 + 'px';
}

function HideSearch() {
    let logo = document.getElementsByClassName("logo")[0];
    let list = document.getElementsByClassName("nav__list")[0];
    let SearchWindow = document.getElementsByClassName("nav__search")[0];
    logo.style.display = "inline-flex";
    list.style.display = "inline-flex";
    SearchWindow.style.display = "none";
    let shadow = document.getElementsByClassName("nav__screen-shadow")[0];
    shadow.style.height = 0;
}
function ShowStates() {
    let btn = document.getElementsByClassName("nav__list__item-hidden__square")[0];
    let btn2 = document.getElementsByClassName("nav__list__item-hidden")[0];
    let item = document.getElementsByClassName("nav__list__item")[0];
    btn.classList.toggle("nav__list__item-hidden__square-visible");
    btn2.classList.toggle("nav__list__item-hidden-visible");
    item.classList.toggle("nav__item-active");

}
function ShowStates2() {
    let btn = document.getElementsByClassName("nav__list__item-hidden__square")[1];
    let btn2 = document.getElementsByClassName("nav__list__item-hidden")[1];
    let item = document.getElementsByClassName("nav__list__item")[4];
    btn.classList.toggle("nav__list__item-hidden__square-visible");
    btn2.classList.toggle("nav__list__item-hidden-visible");
    item.classList.toggle("nav__item-active");
}

function ShowNav() {
    let btn1 = document.getElementsByClassName("nav__mobile-button")[0];
    let btn2 = document.getElementsByClassName("nav__mobile-button-exit")[0];
    let nav_items = document.getElementsByClassName("nav__mobile-items")[0];
    btn1.style.display = "none";
    btn2.style.display = "block";
    nav_items.style.height = "500px";
    nav_items.style.paddingTop = "10px"

}
function HideNav() {
    let btn1 = document.getElementsByClassName("nav__mobile-button")[0];
    let btn2 = document.getElementsByClassName("nav__mobile-button-exit")[0];
    let nav_items = document.getElementsByClassName("nav__mobile-items")[0];
    btn1.style.display = "flex";
    btn2.style.display = "none";
    nav_items.style.height = "0";
    nav_items.style.paddingTop = "0"

}

function ShowState3() {
    let title = document.getElementsByClassName("nav__mobile-item-subtitles")[0];
    title.classList.toggle("nav__mobile-item-subtitles-active1")
}
function ShowState4() {
    let title = document.getElementsByClassName("nav__mobile-item-subtitles")[1];
    title.classList.toggle("nav__mobile-item-subtitles-active2")
}

export default Navbar;