import React from 'react';
import './../style.css'

class AuthorState extends React.Component{

    render() {
        const { data } = this.props;
        let cat = " ";
        if (data.categories[0]  == undefined){

        }
        else
            cat = data.categories[0].name;

        const href = window.location.href.split("/");
        let link;
        if(href[3] == "author")
            link = "../state/" + data.linkName;
        else
            link = "state/" + data.linkName;
        return (
            <a href={link}>
                <div className="author__state">
                    <div className="author__state-date">{data.date.slice(0,10).split("-").join(" ")}</div>
                    <div className="author__state-info">
                        <div className="author__state-category">{ cat}</div>
                        <div className="author__state-title">{data.title}
                        </div>
                        <div className="author__state-description">{data.description}
                        </div>
                    </div>
                </div>
                <div className="author__state-line"></div>
            </a>
        )

    }
}





export default AuthorState;