import React, { Component } from 'react';
import './style.css';
import AuthorState from "./author";
import MoreButton from "../more_button";

class Author extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded1: false,
            items: [],
            states: [],
            count: 0,
            isLoaded2: false,
            allLoaded: false,
            state_count: 3
        };
    }

    componentDidMount() {
        const href = window.location.href.split("/");
        const id = href[href.length - 1];
        fetch('http://thevyshka.styleru.org/api/collab/' + id)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded1: true,
                    items: data
                });
            });
        this.UploadState();

    }


    render() {


        const { error, isLoaded1, items, count} = this.state;
        let photo = "";
        if (!items.photo)
            photo = "https://st2.depositphotos.com/3227229/6685/v/950/depositphotos_66857165-stock-illustration-camera-icon.jpg";
        else
            photo = 'http://thevyshka.styleru.org' + items.photo;

        const background_img = {
            background: 'url('+ photo + ')',
            backgroundSize: 'cover',
            backgroundPosition: 'center'
        };
        if (isLoaded1 && items.id) {

            return (
                <div className="author_page">
                    <div className="page_content">
                        <div className="author__header">
                            <div className="author__img" style={background_img}>
                            </div>
                            <div className="author__header-text">
                                <div className="author__header-text-name">
                                    {items.name}
                                </div>
                                <div className="author__header-text-description">
                                    {items.description}
                                </div>

                            </div>
                            <div className="author__header-stats">
                                <div className="author__header-stats-title">Количество материалов:</div>
                                <div className="author__header-stats-circle">
                                    <div className="author__header-stats-number">{count}</div>
                                </div>
                            </div>
                        </div>

                        <div className="author__states">
                            <div className="author__states-header">Статьи {items.name.split(" ")[items.name.split(" ").length - 2]}:</div>
                            {this.renderAuthors()}
                        </div>
                        <div className="button_container" onClick={() => this.UploadState()}>
                            <div className="button">
                                <div className="button__content">Больше статей</div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        else{
            return (
                <div height='600px'>User not founded</div>
            )
        }
    }
    renderAuthors() {
        const {state, isLoaded2, count, allLoaded} = this.state;

        if (isLoaded2) {
            if (allLoaded) {
                return (
                    state.map((state) => {
                        return (
                            <AuthorState data={state}/>
                        )
                    })
                )
            }
            else {
                return (
                    <div> states not founded </div>
                )
            }
        }
    }
    UploadState(){
        const { state_count } = this.state;
        console.log(this.state.state_count);
        const href = window.location.href.split("/");
        const id = href[href.length - 1];
        let cnt = state_count + 3;
        this.setState({
            state_count: cnt,
        });
        console.log(this.state.state_count);
        fetch('http://thevyshka.styleru.org/api/post/collab/' + id +'/1-' + this.state.state_count )
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded2: true,
                    state: data.posts,
                    count: data.count
                });
            });
        if(this.state.state_count  > this.state.count){
            this.setState({
                allLoaded: true
            });
        }
        console.log(this.state.state_count);
    }
}

export default Author