import React from 'react';
import './style.css'

class MoreButton extends React.Component {
    render() {
        const  text  = this.props.text;
        return (
           <div className="button_container" onClick={this.props.onClick}>
                <div className="button">
                    <div className="button__content">{text}</div>
                </div>
           </div>

        )
    }
}



export default MoreButton;