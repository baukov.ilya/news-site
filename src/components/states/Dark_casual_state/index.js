import React from 'react';
import './../style.css'

class Dark_casual_State extends React.Component{
    render() {
        const { state } = this.props;
        let cat;
        if(state.categories[0] != null)
            cat = state.categories[0].name;
        else
            cat = 100;

        const background_img = {
            background: 'url('+ state.image + ')',
            backgroundSize: 'cover',
            backgroundPosition: 'center'
        };
        return(
            <a href={"/state/" + state.linkName} className={"dark_casual_state"} style={background_img}>
                <div className="state_hover_text">
                    <div className="state_hover_text__type">{cat}</div>
                    <div className="state_hover_text__title">{state.title}</div>
                    <div className="state_hover_text__subtitle">{state.description}</div>
                </div>
                <div className="state_text">
                    <div className={"state_all_text"}>
                        <div className="state_text__type">{cat}</div>
                        <div className="state_text__title">{state.title}</div>
                    </div>
                </div>
            </a>
        )
    }
}




export default Dark_casual_State;