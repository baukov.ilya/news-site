import React from 'react';
import './../style.css'

class News extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded1: false,
            items: [],
        };
    }

    componentDidMount() {
        fetch('http://thevyshka.styleru.org/api/post/category/linkname/news/1-5')
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded1: true,
                    items: data.posts
                });
            });

    }



    render() {
        const { error, isLoaded1, items} = this.state;
        if (isLoaded1) {
            return (
                <div className="news-container">
                    <div className="news__header"> Новости</div>
                    <div className="news__content-container">
                        <div className="news__content">
                            <a href={"state/" + items[0].linkName} className="news__state">
                                <div className="news__state-date">{items[0].date.slice(0,10)}</div>
                                <div className="news__state-title">{items[0].title}</div>
                            </a>
                            <a href={"state/" + items[1].linkName} className="news__state">
                                <div className="news__state-date">{items[1].date.slice(0,10)}</div>
                                <div className="news__state-title">{items[1].title}</div>
                            </a>
                            <a href={"state/" + items[2].linkName} className="news__state">
                                <div className="news__state-date">{items[2].date.slice(0,10)}</div>
                                <div className="news__state-title">{items[2].title}</div>
                            </a>
                            <a href={"state/" + items[3].linkName} className="news__state">
                                <div className="news__state-date">{items[3].date.slice(0,10)}</div>
                                <div className="news__state-title">{items[3].title}</div>
                            </a>
                            <a href={"state/" + items[4].linkName} className="news__state">
                                <div className="news__state-date">{items[4].date.slice(0,10)}</div>
                                <div className="news__state-title">{items[4].title}</div>
                            </a>
                        </div>
                        <div className="news__button" onClick={this.news_wrap}>
                            <svg width="20" height="10" viewBox="0 0 20 10" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1.09222L10.0802 9M9.91977 8.90778L19 1" stroke="black"/>
                            </svg>
                        </div>
                    </div>
                </div>
            )
        }
        else {
            return (
                <div>loading...</div>
            )
        }
    }
//    сut(){
//     let news = document.getElementsByClassName('news__state-title');
//     for (let i = 0; i < 5; i++){
//         while(news[i].textContent.length >= 50){
//             news[i].textContent = news[i].textContent.split(" ").pop();
//         }
//         news[i].textContent = news[i].textContent + '...';
//         console.log(news[i].textContent)
//     }
//     // news.forEach((element) =>{
//     //     while(element.textContent.length >= 50){
//     //         element.split(" ").pop()
//     //     }
//     //     element = element + '...';
//     //     console.log(element)
//     // });
// }
   news_wrap() {
        const content = document.getElementsByClassName("news__content")[0];
        content.classList.toggle("news__wrap")
        const btn = document.getElementsByClassName("news__button")[0];
        btn.classList.toggle("news__button-active")
    }
}


export default News;