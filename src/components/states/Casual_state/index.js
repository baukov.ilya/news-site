import React from 'react';
import './../style.css'

class Casual_State extends React.Component{
    render() {

        const { state } = this.props;
        let cat;
        if(state.categories[0] != null)
            cat = state.categories[0].name;
        else
            cat = 100;

        return(

           <a href={"/state/" + state.linkName} className={"casual_state"}>
               <div className="state_hover_text">
                   <div className="state_hover_text__type">{cat}</div>
                   <div className="state_hover_text__title">{state.title}</div>
                   <div className="state_hover_text__subtitle">{state.description}</div>
               </div>
               <div className="state_img">
                   <img src={state.image} alt=""/>
               </div>
               <div className="state_text">
                   <div className={"state_all_text"}>
                       <div className="state_text__type">{cat}</div>
                       <div className="state_text__title">{state.title}</div>
                   </div>
               </div>
           </a>
        )
    }
}





export default Casual_State;