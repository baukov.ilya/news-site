import React from 'react';
import './style.css'
import StateContent from "./StateContent";
import Author from "../authors/author";

class State extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded1: false,
            items: [],
        };
    }

    componentDidMount() {
        const href = window.location.href.split("/");
        const id = href[href.length - 1];
        fetch('http://thevyshka.styleru.org/api/post/link/' + id)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded1: true,
                    items: data
                });
            });


    }

    render() {

        const { error, isLoaded1, items} = this.state;
        if (isLoaded1 && items.id) {
            console.log(items);
            const background_img = {
                background: 'url('+ items.image + ')',
                backgroundSize: 'cover',
                backgroundPosition: 'center'
            };
            return (
                <div className="page">
                    <div className="state__page">
                        <header className="state__header" style={background_img}>
                            <div className="state__header-container">
                                <a href={"/states/" + items.categories[0].linkName} className="state__header-categories">{items.categories[0].name}</a>
                                <div className="state__header-title">{items.title}</div>
                            </div>
                        </header>
                        <main className="state__content">
                            <div className="state__content-authors">
                                {this.renderAuthors()}
                            </div>
                            <div className="state__content-state">
                                {this.renderStates()}
                            </div>
                        </main>
                    </div>
                </div>

            )
        }
        else{
            return (
                <div height='600px'>Loading...</div>
            )
        }
    }
    renderStates(){
        const { error, isLoaded1, items} = this.state;
        if(isLoaded1){
            return(
                <StateContent state = {items.content}/>
            )
        }

    }
    renderAuthors(){
        const { error, isLoaded1, items} = this.state;
        let photo = "https://st2.depositphotos.com/3227229/6685/v/950/depositphotos_66857165-stock-illustration-camera-icon.jpg";
        if(isLoaded1){
          return(
              items.collaborators.map((item) => {
                  if (item.photo != null){
                      photo = "http://thevyshka.styleru.org" + item.photo;
                  }
                  const background_img = {
                      background: 'url('+ photo + ')',
                      backgroundSize: 'cover',
                      backgroundPosition: 'center'
                  };
                  return(
                      <a href={"../author/" + item.id} className="state__content-author">
                          <div className="state__content-author__img" style={background_img}>
                          </div>
                          <div className="state__content-author__text">
                              <div className="state__content-author__text-name">{item.name}</div>
                              <div className="state__content-author__text-job">{item.role}</div>
                              <div className="state__content-author__description">{item.description}</div>
                          </div>
                      </a>
                  )
              })

          )
      }
    }
}



export default State;