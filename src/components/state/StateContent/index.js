import React from 'react';
import './style.css'

class StateContent extends React.Component {
    componentDidMount() {
        const {state} = this.props;
        document.getElementsByClassName("state_content-container")[0].insertAdjacentHTML('afterbegin', state);
    }

    render() {
        return (
            <div className="state_content-container">
            </div>
        )
    }

}



export default StateContent;