import React, { Component } from 'react';
import './style.css';
import logo from '../pics/TheVyshka logo 2.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faBars} from "@fortawesome/free-solid-svg-icons";
class Footer extends React.Component{


    render() {
            return (
                <div className="footer">
                    <div className="footer__container">
                        <div className="footer_item-container">
                            <div className={"footer__logo"}>
                                <img className="logo" src={logo} alt="logo"></img>
                            </div>
                            <div className="footer__icons">
                                <div className="footer__icon">
                                    <FontAwesomeIcon icon={["fab", "vk"]} />
                                </div>
                                <div className="footer__icon">
                                    <FontAwesomeIcon icon={["fab", "facebook"]} />
                                </div>
                                <div className="footer__icon">
                                    <FontAwesomeIcon icon={["fab", "telegram"]} />
                                </div>
                                <div className="footer__icon">
                                    <FontAwesomeIcon icon={["fab", "twitter"]} />
                                </div>
                                <div className="footer__icon">
                                    <FontAwesomeIcon icon={["fab", "github"]} />
                                </div>
                            </div>
                        </div>
                        <div className="footer__item-container">
                            <div className="footer__state">Новости</div>
                            <div className="footer__state">Мнения</div>
                            <div className="footer__state">Невидимая Вышка</div>
                            <div className="footer__state">Места</div>
                            <div className="footer__state">Истории</div>
                        </div>
                        <div className="footer__item-container">
                            <div className="footer__state">Подкасты</div>
                            <div className="footer__state">Спецпроекты</div>
                            <div className="footer__state">Фотопроекты</div>
                            <div className="footer__state">Авторы</div>
                            <div className="footer__state">О нас</div>
                        </div>
                        <div className="mobile__footer__icons">
                            <div className="footer__icon">
                                <FontAwesomeIcon icon={["fab", "vk"]} />
                            </div>
                            <div className="footer__icon">
                                <FontAwesomeIcon icon={["fab", "facebook"]} />
                            </div>
                            <div className="footer__icon">
                                <FontAwesomeIcon icon={["fab", "telegram"]} />
                            </div>
                            <div className="footer__icon">
                                <FontAwesomeIcon icon={["fab", "twitter"]} />
                            </div>
                            <div className="footer__icon">
                                <FontAwesomeIcon icon={["fab", "github"]} />
                            </div>
                        </div>
                    </div>
                    <div className="footer__copyright">
                        <div className="footer__copyright-text">
                            &copy; The Vyshka 2020
                        </div>
                    </div>
                </div>
            )
    }


}

export default Footer