import React, { Component } from 'react';
import Author from "./author";

class Authors extends React.Component{


    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded1: false,
            items: [],
            state_count: 16
        };
    }

    componentDidMount() {
        fetch('http://thevyshka.styleru.org/api/collab/1-10')
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                console.log(data)
                this.setState({
                    isLoaded1: true,
                    items: data.collaborators
                });
            });
    }

    render() {
        const { error, isLoaded1, items} = this.state;
        console.log(items);

        if(isLoaded1) {
            return (
                <div className="states_page">
                    <div className="authors_page">
                        <div className="authors__header">
                            <div className="authors__header__text">Редакция</div>
                            <div className="authors__header__search">
                                <input className="authors__header__search-input" placeholder="Имя Фамилия"/>
                                <div className="authors__header__search-button" onClick={() => this.SearchUsers()}>
                                    <svg width="20" height="19" viewBox="0 0 20 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M9 15.8593C13.4183 15.8593 17 12.5329 17 8.42966C17 4.32637 13.4183 1 9 1C4.58172 1 1 4.32637 1 8.42966C1 12.5329 4.58172 15.8593 9 15.8593Z" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        <path d="M18.9999 17.7168L14.6499 13.6769" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    </svg>

                                </div>
                            </div>
                        </div>
                        <div className="authors__container">
                            {this.renderAuthors()}
                        </div>
                        <div className="button_container" onClick={() => this.UploadState()}>
                            <div className="button" >
                                <div className="button__content">Больше авторов</div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        else{
            return (
                <div className="loading">loading...</div>
            )
        }

    }
    renderAuthors(){
        const { error, isLoaded1, items} = this.state;
        return(
            items.map((item) =>{
                return(
                    <Author author = {item}/>
                )
            })

        )
    }
    UploadState(){
        const { state_count } = this.state;
        let cnt = state_count + 6;
        this.setState({
            state_count: cnt,
        });
        fetch('http://thevyshka.styleru.org/api/collab/1-' + this.state.state_count )
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded2: true,
                    items: data.collaborators,
                });
            });
    }
    SearchUsers(){
        let user = document.getElementsByClassName("authors__header__search-input")[0].value;
        console.log()
        this.setState({
            state_count: 10,
        });
        fetch('http://thevyshka.styleru.org/api/collab/search/'+ user +'/1-10' )
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded2: true,
                    items: data.collaborators,
                });
            });
    }
}

export default Authors