import React from 'react';
import './../style.css'

class Author extends React.Component{
    render() {
        const { author } = this.props;
        let photo = "";
        if (!author.photo)
            photo = "https://st2.depositphotos.com/3227229/6685/v/950/depositphotos_66857165-stock-illustration-camera-icon.jpg";
        else
            photo = 'http://thevyshka.styleru.org' + author.photo;

        const link = "/author/" + author.id;

        const background_img = {
            background: 'url('+ photo + ')',
            backgroundSize: 'cover',
            backgroundPosition: 'center'
        };
        return(
            <a href = {link}>
              <div className={"author"}>
                  <div className="author__content">
                      <div className="author__picture">
                          <div className="author__picture-img" style={background_img}>
                          </div>
                      </div>
                      <div className="author__text">
                          <div className="author__text-name">{author.name}</div>
                          <div className="author__text-description">{author.description}</div>
                          <div className="author__text-job">
                              {this.renderJobSubject()}
                          </div>
                      </div>
                  </div>
              </div>
            </a>

        )
    }
    renderJobSubject(){
        const { author } = this.props;
        if (author.roles !== null){
            const roles  = author.roles;
            return(
                roles.map((roleel) =>{
                    return(
                        <div className="author__text-job__subject">{roleel}</div>
                    )
                })

            )
        }
        else{
            return (
                <div className="author__text-job__subject">Ролей нет...</div>
            )
        }
    }
}




export default Author;